from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn
import pymongo
import mysql.connector as mc

class Client(BaseModel) :
    lName: str
    fName: str
    mail: str
    nbOrder: int

class Produit(BaseModel) :
    name: str
    descriptif: str
    quantite: int

app = FastAPI()

db = mc.connect( # Connexion to database
        host = "localhost", # host
        user = "api", # user
        password = "api", # password
        database= "client" # database
    )

cur = db.cursor()

cur.execute("SHOW TABLES")
if cur.fetchall() == [] : # if no tables
    cur.execute("""
CREATE TABLE IF NOT EXISTS Client(
    lName VARCHAR(100),
    fName VARCHAR(100),
    mail VARCHAR(100) PRIMARY KEY,
    nbOrder INT
);
    """)

client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["produits"]
col = db["produits"]

@app.get("/")
def root() :
    return {"message": "bonjour"}

@app.post("/clients/")
def add_client(client: Client) :
    data = client.dict()
    sql = "INSERT INTO Client(lName, fName, mail, nbOrder) VALUES (%s, %s, %s, %s)"
    val = (data["lName"], data["fName"], data["mail"], data["nbOrder"])
    cur.execute(sql, val)
    db.commit()

@app.get("/clients/")
def get_clients() :
    cur.execute("SELECT * FROM Client")
    res = cur.fetchall()
    return [{"lName":c[0], "fName":c[1], "mail":c[2], "nbOrder":c[3]} for c in res]

@app.delete("/clients/")
def delete_clients() :
    cur.execute("DELETE * FROM Client")
    db.commit()

@app.get("/clients/{mail}")
def get_client(mail: str) :
    sql = "SELECT * FROM Client WHERE mail = %s"
    val = (mail,)
    cur.execute(sql, val)
    res = cur.fetchone()
    return {"lName":res[0], "fName":res[1], "mail":res[2], "nbOrder":res[3]}

@app.put("/clients/{mail}")
def update_produit(mail: str, client: Client) :
    data = client.dict()
    sql = "SELECT * FROM Client WHERE mail = %s"
    val = (mail,)
    cur.execute(sql, val)
    res = cur.fetchone()
    if res == None :
        sql = "INSERT INTO Client(lName, fName, mail, nbOrder) VALUES (%s, %s, %s, %s)"
        val = (data["lName"], data["fName"], data["mail"], data["nbOrder"])
    else :
        sql = "UPDATE Client SET lName = %s, fName = %s, nbOrder = %s WHERE mail = %s"
        val = (data["lName"], data["fName"], data["nbOrder"], data["mail"])
    cur.execute(sql, val)
    db.commit()

@app.delete("/clients/{mail}")
def delete_produit(mail: str) :
    sql = "DELETE * FROM Client WHERE mail = %s"
    val = (mail,)
    cur.execute(sql, val)

@app.post("/produits/")
def add_produit(produit: Produit) :
    col.insert_one(produit.dict())

@app.get("/produits/")
def get_produits() :
    return col.find()

@app.delete("/produits/")
def delete_produits() :
    col.delete_many({})

@app.get("/produits/{name}")
def get_produit(name: str) :
    return col.find_one({"name" : name})

@app.put("/produits/{name}")
def update_produit(name: str, produit: Produit) :
    col.update_one({"name" : name}, {"$set" : produit.dict()})

@app.delete("/produits/{name}")
def delete_produit(name: str) :
    col.delete_one({"name": name})